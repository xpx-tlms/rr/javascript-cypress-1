/// <reference types="cypress" />

import { add } from '../../src/calculator'

describe('Unit testing', () => {
  it('adds', () => {
    const result = add(1,2)
    expect(result).equal(3)
  })
})
