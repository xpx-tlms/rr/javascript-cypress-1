/// <reference types="cypress" />

/// <reference types="cypress" />

describe("searching", () => {

    before(() => { // same as before all
        cy.visit('http://127.0.0.1:5500/src/index.html')
    })

    // it('found it', () => {
    //     cy.contains('div', 'Hello World')
    // })

    it('found it', () => {
        // GET CSS Selectors.
        cy.get('.todoList_header').find('button', 'Manage').click()         // class selector
        //cy.get('#footer').click() // or      cy.get('div#footer').click() // id selector
        // cy.get('div[data-tid="001"]')                                   // attribute selector
        // cy.get('.content').contains('The air is bad.')
        // cy.get('button:contains("Manage")').eq(1).click()

        // DOM.
        // cy.contains('button', 'Manage').click()
        // cy.contains('div', 'Cars').next().contains('li', "Mazda")
        // cy.contains('div', 'Cars').next().find('li').eq(1)
        // cy.contains('div', 'parent').children()
    })
})
