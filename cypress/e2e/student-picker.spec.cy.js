/// <reference types="cypress" />

describe('Student Picker', () => {
    // Arrange
    before(() => {
        cy.visit('http://student-picker.s3-website-us-east-1.amazonaws.com/')
    })

    it('Add John Doe as a new Student', () => {
      // Act
      cy.get('a[href="/manage"]').click(); 
      cy.get('input[placeholder="Student Name"]').type('John Doe');
      cy.contains('button', 'Add').click(); 

      // Assert
      cy.contains('td', 'John Doe');
    })
})
