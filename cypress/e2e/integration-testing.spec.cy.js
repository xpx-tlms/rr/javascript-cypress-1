/// <reference types="cypress" />

describe('API', () => {
    it('GET /user/1', async () => {
        // Arrange
        const url = 'https://jsonplaceholder.typicode.com/users/1'

        // Act
        let res = await cy.request(url)
        
        // Assert
        expect(res.status).to.eq(200)
        expect(res.body).to.have.property('id')
        expect(res.body.name).to.eq('Leanne Graham')
        expect(res.body.email).to.eq('Sincere@april.biz')
    })

    it('POST /posts', async () => {
        // Arrange
        const request = {
            method: "POST",
            url: 'https://jsonplaceholder.typicode.com/posts',
            body: {
                "title": "foo", 
                "body": "bar",
                "userId": 1
            }
        }
        
        // Act
        let res = await cy.request(request)
       
        // Assert
        const id = res.body.id
        expect(res.status).to.eq(201)
        expect(res.body).to.have.property('id')
        expect (id).to.eq(101)
        cy.log(JSON.stringify(res.body))
    })

    // it('Fixture test', async () => {
    //     let user = await cy.fixture('user.json')
    //     cy.intercept('GET', 'https://jsonplaceholder.typicode.com/users/1', user).as('getUser')
    //     await cy.request('GET', 'https://jsonplaceholder.typicode.com/users/1')
    //     let r = await cy.wait('@getUser')
    //     cy.log(r)
    // })
})