# JavaScript Cypress 1
Reference repo to help us understand Cypress.

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Open desktop app: `npx cypress open`

# Cypress Notes
- Open desktop app: `npx cypress open`
- Command Line: https://docs.cypress.io/guides/guides/command-line
- API: https://docs.cypress.io/api/table-of-contents/
- Disable video recording: `cypress run --config video=false`

# Selecting
- https://docs.cypress.io/api/commands/contains => Get the DOM element containing the text. 
- https://docs.cypress.io/api/commands/get => Get one or more DOM elements by selector or alias.
- https://docs.cypress.io/api/commands/find => Get the descendent DOM elements of a specific selector.


### Selector Schema
- {HTML Element Type}[{attribute}="{value}"]
- Example: `input[placeholder="Student Name"]`


The cy.get command always starts its search from the cy.root element. In most cases, it is the document element, unless used inside the .within() command. The .find command starts its search from the current subject.


# Setting up Cypress
- `npm install cypress --save-dev`
- `npx cypress open --e2e` ==> E2E testing

# FYI
- describe & it come from Mocha
- expect comes from Chai

# ProTip
Add this to your TypeScript spec files:
```
/// <reference types="cypress" />
```

# Code Coverage
- `npm install --save-dev @cypress/code-coverage istanbul-lib-coverage`
- e2e.js : import '@cypress/code-coverage/support';
- npx nyc instrument --compact=false src instrumented

